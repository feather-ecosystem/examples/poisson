using UnivariateSplines, AbstractMappings, KroneckerProducts, CartesianProducts, TensorProductBsplines
using LinearAlgebra, StaticArrays

using Poisson, SpecialFunctions

# domain and the mapping in polar coordinates
dom = Interval(11.064709488501170, 17.61596604980483) ⨱ Interval(0.0,2*π);
mapping = GeometricMapping(dom, (r,θ) -> r * cos(θ), (r,θ) -> r * sin(θ));

# analytical solution
uₐ = ScalarFunction((r, θ) -> besselj(4, r) * cos(4θ))

bodyforce = (x,y) -> uₐ(sqrt.(x.^2+y.^2), atan.(y,x));    # -Δuₐ = f
material = (x,y) -> SMatrix{2,2,Float64}(I);  # conductivity tensor
traction = (x,y) -> @SVector [0.0; 0.0]

# define the partitioning into elements
partition = CartesianProduct((d,n) -> IncreasingRange(d,n), dom, (21,51));

# test and trial-space
order = 4
trialspace = TensorProduct(
        SplineSpace(order, partition.data[1]; cleft=[1], cright=[1]),
        SplineSpace(order, partition.data[2]; cperiodic=1:order)        # Cᵖ⁻¹ periodic splinespace 
    );

# create discrete solution field
solution = Field(TensorProductBspline, trialspace);

# solve step
directsolve!(solution=solution, mapping=mapping, material=material, bodyforce=bodyforce, traction=traction);

# check l2-error
using Test
@test l2_error(solution, to=uₐ, relative=true)[1] < 1e-5

# plot result
using GLMakie
GLMakie.activate!()

fig = Figure(resolution = (1200, 800))
ax = LScene(fig[1, 1], show_axis = true)

# get visualization points in parametric space
X = visualization_grid(mapping, density=(101,201));
@evaluate Y = mapping(X);
@evaluate u = solution(X);

# plot shell - color denotes the z-variable
x, y = map(Matrix, Y.data)
GLMakie.surface!(ax, x, y, 10 .* u, color = u, colormap = :jet)