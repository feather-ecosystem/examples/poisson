using UnivariateSplines, AbstractMappings, KroneckerProducts, CartesianProducts, TensorProductBsplines, IgaFormation
using LinearAlgebra, StaticArrays, LinearMaps, Krylov

using Poisson, SpecialFunctions

# Domain and the mapping in polar coordinates. The interval is chosen such that integrals
# coincides with the zeros of the 1st Bessel function.
dom = Interval(11.064709488501170, 17.61596604980483) ⨱ Interval(0.0,2*π);
mapping = GeometricMapping(dom, (r,θ) -> r * cos(θ), (r,θ) -> r * sin(θ));

# analytical solution
uₐ = ScalarFunction((r, θ) -> besselj(4, r) * cos(4θ))
fₐ = uₐ

bodyforce = (x,y) -> uₐ(sqrt.(x.^2+y.^2), atan.(y,x));    # -Δuₐ = f
material = (x,y) -> SMatrix{2,2,Float64}(I);  # conductivity tensor
traction = (x,y) -> @SVector [0.0; 0.0];

# define the partitioning into elements
partition = CartesianProduct((d,n) -> IncreasingRange(d,n), dom, (11,26));

# test and trial-space
order = 5
trialspace = TensorProduct(
        SplineSpace(order, partition.data[1]; cleft=1:1, cright=1:1),
        SplineSpace(order, partition.data[2]; cperiodic=1:order)        # Cᵖ⁻¹ periodic splinespace 
    );

# create discrete solution field
solution = Field(TensorProductBspline, trialspace);

# setup pde-problem
problem = PDEProblem(solution=solution, mapping=mapping, material=material, bodyforce=bodyforce);
A = LinearMapping(problem)
b = eval_rhs!(problem);

# setup preconditioner
P = FastDiagonalization(solution)
Pg = FastDiagonalization(solution, A)

Ā, P̄, P̄g = Matrix.((A,P,Pg))
κ_A, κ_PA, κ_PgA = cond.((Ā, P̄*Ā, P̄g*Ā))

# solve using the (preconditioned) conjugate gradient method
@time x, cg_log = Krylov.cg(A, b, rtol=1e-8; M=Pg)
@time x, cg_log_nogeo = Krylov.cg(A, b, rtol=1e-8; M=P)
@time x, cg_log_nopre = Krylov.cg(A, b, rtol=1e-8)

@info "Numer of iterations with preconditioning and diagonal scaling: " cg_log.niter
@info "Numer of iterations with preconditioning and no diagonal scaling: " cg_log_nogeo.niter
@info "Numer of iterations without preconditioning: " cg_log_nopre.niter

solution[1].coeffs[:] = x;

# check l2-error
using Test
@test l2_error(solution, to=uₐ, relative=true)[1] < 1e-3
@test κ_A > κ_PA > κ_PgA

# plot result
using GLMakie
GLMakie.activate!()

fig = Figure(resolution = (1200, 800))
ax = LScene(fig[1, 1], show_axis = true)

# get visualization points in parametric space
X = visualization_grid(mapping, density=(101,201));
@evaluate Y = mapping(X);
@evaluate u = solution(X);

# plot shell - color denotes the z-variable
x, y = map(Matrix, Y.data)
GLMakie.surface!(ax, x, y, 10 .* u, color = u, colormap = :jet)