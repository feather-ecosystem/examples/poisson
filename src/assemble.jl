export assemble, kronecker_approximation

function assemble(; space, quadrule, mapping, material, bodyforce, traction)

    # extract partition
    partition = CartesianProduct(s -> breakpoints(s), space)

    # create an element accessor
    acc = ElementAccessor(testspace=space, trialspace=space, quadrule=quadrule, incorporate_weights_in_testfuns=true);

    # create cache for sum factorization
    mat_cache = MatrixSumfactoryCache(acc);
    vec_cache = VectorSumfactoryCache(acc);

    # create pull-back of material law
    pullback_body!(e) = pullback_body_data!(acc, e, FieldEvaluationCache(acc, 2, 2), FieldEvaluationCache(acc, 1, 1), mapping, material, bodyforce)

    # allocate space for global mass matrix
    m = prod(s -> KroneckerProducts.size(s.C,1), space)
    K = spzeros(m, m)
    f = zeros(m)

    # loop over elements
    for element in Elements(partition)

        # get global indices
        A = TestIndices(acc, element)
        B = TrialIndices(acc, element)

        # compute transformation due to material and geometry
        Cₑ, Fₑ = pullback_body!(element)

        # compute trial function derivatives
        ∇u = (TrialFunctions(acc, element; ders=(1,0)), TrialFunctions(acc, element; ders=(0,1)))

        # compute test functions
        v  = TestFunctions(acc, element; ders=(0,0))
        ∇v = (TestFunctions(acc, element; ders=(1,0)), TestFunctions(acc, element; ders=(0,1)))

        # compute element stiffness matrix
        ∫ = Sumfactory(mat_cache, element)
        for β=1:2
            for α=1:2
                ∫(∇u[α], ∇v[β]; data=Cₑ.data[α,β], reset=false)
            end
        end

        # add to global mass matrix
        K[A, B] += ∫.data[1]

        # compute element contribution of the right-hand-side
        ∫ = Sumfactory(vec_cache, element)

        # add to global rhs vector
        f[A] += ∫(v; data=Fₑ.data[1])
    end
 

    # form boundary traction integrals 

    # loop over boundaries
    for side in 1:4

        # define pull-back of traction vector on current boundary component
        pullback_boundary!(e) = pullback_boundary_data!(acc, e, 
                FieldEvaluationCache(acc), boundary(mapping, side), traction)

        # loop over boundary elements
        for element in Elements(restriction(partition, side))
        
            # get global indices
            A = TestIndices(acc, element)

            # compute test function derivatives at the quadrature points
            v = TestFunctions(acc, element; ders=(0,0))

            # compute the pulled-back material data
            Fₜ = pullback_boundary!(element)

            #get sumfactorization object
            ∫ = Sumfactory(vec_cache, element)

            #compute contribution to force vector
            ∫(v; data=Fₜ.data[1])
            
            # add to global stiffness matrix
            f[A] += ∫.data[1]

        end # element loop

    end # all boundaries

    return K, f
end


function kronecker_approximation(field::Field{2,1,1,T}) where T
    C₁, C₂ = field[1].space[1].C, field[1].space[2].C
    V₁, V₂ = field[1].space
    
    M₁ = Matrix(C₁' * system_matrix(V₁, V₁, 1, 1) * C₁)
    K₁ = Matrix(C₁' * system_matrix(V₁, V₁, 2, 2) * C₁)
    M₂ = Matrix(C₂' * system_matrix(V₂, V₂, 1, 1) * C₂)
    K₂ = Matrix(C₂' * system_matrix(V₂, V₂, 2, 2) * C₂)

    return M₁, M₂, K₁, K₂
end