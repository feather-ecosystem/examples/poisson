export pullback_body_data_matrixfree!, pullback_forcing_data_matrixfree!

function pullback_body_data_matrixfree!(cache, X, Y, ∇Y, u, κ)

    # evaluate jacobian of the mapping at all points
    @evaluate! cache = Gradient(u)(X) # the cache is reused
    
    # pull back transformation at each quadrature point
    for k in eachindex(cache)
            
        # physical coordinates
        y = Y[k]

        # Jacobian at quadrature point (i,j)
        J = ∇Y[k]

        # get the gradient of the solution
        ∇u = cache[k]

        # compute and save material-geometry contribution for stiffness
        cache[k] = inv(J)' * κ(y...) * (inv(J) * ∇u) * det(J)
    end
end

function pullback_body_data_matrixfree!(acc::PatchAccessor, cache_k::FieldEvaluationCache, Y, ∇Y, u, κ)
    patch_cache_k = extract_patch_cache(cache_k)
    X = QuadraturePoints(acc)
    pullback_body_data_matrixfree!(patch_cache_k, X, Y, ∇Y, u, κ)
    return patch_cache_k
end

function pullback_forcing_data_matrixfree!(cache, Y, ∇Y, f)

    # pull back transformation at each quadrature point
    for k in eachindex(cache)
            
        # physical coordinates
        y = Y[k]

        # Jacobian at quadrature point (i,j)
        J = ∇Y[k]
        
        # multiply rhs with Jacobian determinant
        cache[k] = f(y...) * det(J)
    end
end

function pullback_forcing_data_matrixfree!(acc::PatchAccessor, cache_f::FieldEvaluationCache, Y, ∇Y, f)
    patch_cache_f = extract_patch_cache(cache_f)
    pullback_forcing_data_matrixfree!(patch_cache_f, Y, ∇Y, f)
    return patch_cache_f
end