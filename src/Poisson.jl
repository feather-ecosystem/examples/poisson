module Poisson

using IgaBase, AbstractMappings
using SortedSequences, UnivariateSplines, TensorProductBsplines, NURBS
using CartesianProducts, KroneckerProducts
using IgaFormation

using LinearAlgebra, SparseArrays, LinearMaps, Krylov, StaticArrays

import UnivariateSplines.system_matrix

include("constitutive.jl")
include("assemble.jl")
include("solve.jl")

include("constitutive-matrixfree.jl")
include("assemble-matrixfree.jl")

include("preconditioner.jl")

end # module
