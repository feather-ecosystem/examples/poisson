export PDEProblem, eval_forward_problem!, eval_rhs!, LinearMapping

struct PDEProblem{F<:Field, A<:Accessor, V<:VectorSumfactoryCache, B<:KroneckerProduct}
    solution::F
    acc::A
    pullback_body::Function
    pullback_forcing::Function
    sumfact_cache_u::V
    sumfact_cache_f::V
    testfuns::Tuple{B,B,B}
end

function PDEProblem(; solution, mapping, material, bodyforce)

    # get solution space
    space = solution[1].space

    # extract partition from the solution space
    partition = CartesianProduct(s -> breakpoints(s), space)

    # define regular quadrature rule, element accessor, and element sum-factorization cache
    quadrule = TensorProduct((d, u) -> PatchRule(d; npoints=ceil(Int, Degree(u)+1), method=Legendre), partition, space)

    # create a patch accessor
    acc = PatchAccessor(testspace=space, trialspace=space, quadrule=quadrule, incorporate_weights_in_testfuns=true)

    # create caches for sum factorization
    sumfact_cache_u = VectorSumfactoryCache(acc)
    sumfact_cache_f = VectorSumfactoryCache(acc)

    # compute test-functions and derivatives
    testfuns = (TestFunctions(acc; ders=(0,0)), TestFunctions(acc; ders=(1,0)), TestFunctions(acc; ders=(0,1)))

    # precompute mapping data
    X = QuadraturePoints(acc)
    @evaluate Y = mapping(X)
    @evaluate ∇Y = Gradient(mapping)(X)
    
    # create pull-back operator
    pullback_body!(solution) = pullback_body_data_matrixfree!(acc, FieldEvaluationCache(acc, 2, 1), Y, ∇Y, solution, material)
    pullback_forcing!() = pullback_forcing_data_matrixfree!(acc, FieldEvaluationCache(acc, 1, 1), Y, ∇Y, bodyforce)

    return PDEProblem(solution, acc, pullback_body!, pullback_forcing!, sumfact_cache_u, sumfact_cache_f, testfuns)
end

size(P::PDEProblem) = (length(P.solution[1].coeffs), length(P.solution[1].coeffs))


# evaluate the forward problem
function eval_forward_problem!(P::PDEProblem)

    # get solution field
    u = P.solution

    # create integration object
    ∫ = Sumfactory(P.sumfact_cache_u)

    # pullback solution data at quadrature points
    C = P.pullback_body(u)

    # get test functions
    ∇v = P.testfuns[2:3]

    # perform quadrature by sum factorization
    ∫(∇v[1]; data=C.data[1], reset=false)
    ∫(∇v[2]; data=C.data[2], reset=false)
    
    return ∫.data[1]
end

# evaluate the forward problem
function eval_rhs!(P::PDEProblem)

    # create integration object
    ∫ = Sumfactory(P.sumfact_cache_f)

    # pullback solution data at quadrature points
    C = P.pullback_forcing()

    # get test functions
    v = P.testfuns[1]

    # perform quadrature
    ∫(v; data=C.data[1])
    
    return ∫.data[1]
end


# construct a linear mapping corresponding to this PDE-problem
struct LinearMapping{P<:PDEProblem} <: LinearMaps.LinearMap{Float64}
    problem::P
end

Base.size(A::LinearMapping) = size(A.problem)
LinearAlgebra.issymmetric(::LinearMapping) = true
LinearAlgebra.ishermitian(::LinearMapping) = true
LinearAlgebra.isposdef(::LinearMapping) = true

function LinearMaps._unsafe_mul!(b::AbstractVector, A::LinearMapping, x::AbstractVector)

    # update solution dofs
    A.problem.solution[1].coeffs[:] = x
    
    # evaluate forward problem and update `b`
    b .= eval_forward_problem!(A.problem)
end




