export FastDiagonalization

struct FastDiagonalization{T} <: LinearMaps.LinearMap{T} 
    size::NTuple{2,Int}
    Λ::Diagonal{T, Vector{T}}
    U::KroneckerProduct

    function FastDiagonalization(field::Field{2,1,1,T}) where T
        M₁, M₂, K₁, K₂ = kronecker_approximation(field)

        EVD₁ = eigen(Symmetric(K₁), Symmetric(M₁))
        EVD₂ = eigen(Symmetric(K₂), Symmetric(M₂))

        λ₁, u₁ = EVD₁.values, EVD₁.vectors
        λ₂, u₂ = EVD₂.values, EVD₂.vectors

        U = KroneckerProduct(u₂, u₁)
        Λ = Diagonal(KroneckerSum(λ₂, λ₁))

        sz = Base.size(U)
        Λ = Diagonal(inv.(Λ.diag))

        return new{eltype(Λ)}(sz, Λ, U)
    end

    function FastDiagonalization(field::Field{2,1,1,T}, linmap) where T
        M₁, M₂, K₁, K₂ = kronecker_approximation(field)

        EVD₁ = eigen(Symmetric(K₁), Symmetric(M₁))
        EVD₂ = eigen(Symmetric(K₂), Symmetric(M₂))

        u₁ = EVD₁.vectors
        u₂ = EVD₂.vectors

        U = KroneckerProduct(u₂, u₁)
        sz = Base.size(U)

        Λ̃ = (U' * linmap * U) * ones(Base.size(linmap,2))
        @assert all(Λ̃ .> 0)

        Λ = Diagonal(inv.(Λ̃))

        return new{eltype(Λ)}(sz, Λ, U)
    end
end

Base.size(op::FastDiagonalization) = op.size
LinearAlgebra.issymmetric(::FastDiagonalization) = true
LinearAlgebra.ishermitian(::FastDiagonalization) = true
LinearAlgebra.isposdef(::FastDiagonalization) = true

function LinearMaps._unsafe_mul!(b::AbstractVecOrMat, op::FastDiagonalization, v::AbstractVector)
    b .= op.U' * v
    b .= op.Λ * b
    b .= op.U * b
end
