export directsolve!

function directsolve!(; solution, mapping::GeometricMapping, material, bodyforce, traction)

    # get solution space
    space = solution[1].space

    # extract partition from the solution space
    partition = CartesianProduct(s -> breakpoints(s), space)

    # define regular quadrature rule, element accessor, and element sum-factorization cache
    quadrule = TensorProduct((d, u) -> PatchRule(d; npoints=ceil(Int, Degree(u)+1), method=Legendre), partition, space);

    # assemble stiffness matrix and forcing vector
    @info "Formation and assembly of matrix-equation."
    K, f = assemble(space=space, quadrule=quadrule, mapping=mapping, material=material, bodyforce=bodyforce, traction=traction);

    # apply clamped boundary conditions strongly
    @info "Application of Dirichlet boundary constraints."
    A, b = apply_clamped_boundary_conditions(K, f, solution)
   
    # solve final system
    @info "Solve system of equations."
    solution[1].coeffs[:] .= A \ b

    return nothing
end

function apply_clamped_boundary_conditions(K, f, solution)

    # extraction operators
    C = sparse(KroneckerProduct(solution[1].space[2].C, solution[1].space[1].C))
    
    # assemble global system matrix
    A = C' * K * C
    
    # assmble global force vector
    b = C' * f
    
    return A, b
end